package com.teamx.ticketingkai;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


public class MainActivity extends AppCompatActivity {
    private EditText email, passkey;
    private Button login;
    SQLiteDatabase db;
    SQLiteOpenHelper openHelper;
    Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = (EditText) findViewById(R.id.edEmail);
        passkey = (EditText) findViewById(R.id.edPassword);
        openHelper=new DatabaseHelper(this);
        db = openHelper.getReadableDatabase();
    }

    public void setupMain(){

    }

    public void signUp(View view){
        Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
        startActivity(intent);
    }

    public void signIn(View view){

        validate(email.getText().toString(),passkey.getText().toString());
    }

    public void validate(String mail, String pass){


try{
        cursor = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_NAME + " WHERE " + DatabaseHelper.COL_4 + "=? AND " + DatabaseHelper.COL_3 + "=?", new String[]{mail, pass});
    Toast.makeText(MainActivity.this,"login ... \n "+ mail.trim() , Toast.LENGTH_SHORT).show();
    if (cursor != null) {
        if (cursor.getCount() > 0) {
        Toast.makeText(getApplicationContext(), "Login Success ", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this,Home.class);
        intent.putExtra("username",mail);
        intent.putExtra("pass",pass);
        email.setText("");
        passkey.setText("");
        startActivity(intent);

        } else {
        Toast.makeText(getApplicationContext(), "Login error", Toast.LENGTH_SHORT).show();
        }
    }


}catch(Exception e){

}


    }


}


