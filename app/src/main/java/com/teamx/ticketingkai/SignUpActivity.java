package com.teamx.ticketingkai;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import org.w3c.dom.Text;

public class SignUpActivity extends AppCompatActivity {

    private TextView errorMessage;
    private EditText username,email,pass;
    SQLiteOpenHelper openHelper;
    SQLiteDatabase db;
    //Button _btnreg, _btnlogin;
    EditText _txtfanme, _txtlname, _txtpass, _txtemail, _txtphone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        errorMessage = (TextView) findViewById(R.id.errorMessageUp);
        setup();


    }
    public void cancel (View view){
        finish();
    }

    private void setup(){
        username = (EditText) findViewById(R.id.edName);
        email = (EditText) findViewById(R.id.edEmailsUP);
        pass = (EditText) findViewById(R.id.edPassUp);
        openHelper = new DatabaseHelper(this);

    }

    public void signUp(View view){
        if (isiCheck()){
            db=openHelper.getWritableDatabase();
            String name, userEmail, passkey;
            name = username.getText().toString();
            userEmail = email.getText().toString();
            passkey = pass.getText().toString();
            insertdata(name,passkey,userEmail);
            Toast.makeText(SignUpActivity.this,"Registration Success", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void insertdata(String name, String pass, String email){
    ContentValues contentValues = new ContentValues();
    contentValues.put(DatabaseHelper.COL_2, name);
    contentValues.put(DatabaseHelper.COL_3, email);
    contentValues.put(DatabaseHelper.COL_4, pass);

    long id = db.insert(DatabaseHelper.TABLE_NAME, null, contentValues);
}



    public boolean isiCheck(){
        if (username.getText().toString().isEmpty()||email.getText().toString().isEmpty()||pass.getText().toString().isEmpty()){
            Toast.makeText(SignUpActivity.this,"Isi semua data", Toast.LENGTH_SHORT).show();
            return false;
        } else{
            return true;
        }
    }
}
